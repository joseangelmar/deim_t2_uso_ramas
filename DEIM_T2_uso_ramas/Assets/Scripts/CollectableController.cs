using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Coleccionable recogido");
        Destroy(gameObject);
    }
}
