using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public void PlayLevel()
    {
        // Carga la escena de juego
        SceneManager.LoadScene("Game");
    }

}
